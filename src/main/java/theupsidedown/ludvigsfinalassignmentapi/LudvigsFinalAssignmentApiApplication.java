package theupsidedown.ludvigsfinalassignmentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LudvigsFinalAssignmentApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LudvigsFinalAssignmentApiApplication.class, args);
    }

}
