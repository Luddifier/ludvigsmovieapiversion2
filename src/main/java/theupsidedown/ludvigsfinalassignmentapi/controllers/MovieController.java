package theupsidedown.ludvigsfinalassignmentapi.controllers;

import theupsidedown.ludvigsfinalassignmentapi.domain.Movie;
import theupsidedown.ludvigsfinalassignmentapi.Repository.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


//Controller, CRUD for Movies, see Swagger-tags for details
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movie")

public class MovieController {

    private final MovieRepository movieRepo;


    public MovieController(MovieRepository movieRepo) {
        this.movieRepo = movieRepo;

    }


    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){

        List<Movie> movies = movieRepo.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    @Operation(summary = "Get specific movie by id")
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getSpecificMovie(@PathVariable Long id){
        HttpStatus status;
        Movie getMovie = new Movie();
        if (!movieRepo.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(getMovie, status);
        }
        getMovie = movieRepo.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(getMovie, status);
    }

    @Operation(summary = "Update a specific movie using ID and RequestBody")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie movie, @PathVariable Long id){
        HttpStatus status;
        Movie returnMovie = new Movie();
        if(!Objects.equals(id, movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepo.save(movie);
        status = HttpStatus.ACCEPTED;
        return new ResponseEntity<>(returnMovie,status);
    }

    @Operation(summary = "Update a specific movie's set of characters, using Movie ID and ID's for characters as RequestBody")
    @PatchMapping("/{id}")
    public ResponseEntity<Movie> updateSetOfCharacters(@RequestBody Movie setOfCharacters, @PathVariable Long id){
        try {
            Movie movie = movieRepo.getById(id);
            movie.setCharacters(setOfCharacters.getCharacters());
            return new ResponseEntity<>(movieRepo.save(movie), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @Operation(summary = "Add a specific movie to the database, matching RequestBody")
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        Movie returnMovie = movieRepo.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    @Operation(summary = "Delete a specific movie using ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable Long id){
        HttpStatus status;
        if(movieRepo.existsById(id)){
            status = HttpStatus.OK;
            movieRepo.deleteById(id);
            return new ResponseEntity<>("Movie with ID: " + id + " has been deleted.", status);
        }
        status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>("No movie with that id exists", status);
    }

/*    @Operation(summary = "Get all characters from a specific movie using ID")
    @GetMapping("/characters/{id}")
    public ResponseEntity<AllCharactersFromMovieDto> allCharactersFromMovie(@PathVariable Long id){
        if (movieRepo.existsById(id)){
            var returnList = allCharactersFromMovieDtoMapper.getAllCharacters(movieRepo.getById(id));
            return new ResponseEntity<>(returnList, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }*/
}


