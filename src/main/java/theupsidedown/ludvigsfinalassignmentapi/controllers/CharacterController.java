package theupsidedown.ludvigsfinalassignmentapi.controllers;

import theupsidedown.ludvigsfinalassignmentapi.domain.MovieCharacter;
import theupsidedown.ludvigsfinalassignmentapi.Repository.CharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Controller, CRUD for Characters, see Swagger-tags for details
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")

public class CharacterController {

    private final CharacterRepository characterRepository;

    @Autowired
    public CharacterController(CharacterRepository characterRepo) {
        this.characterRepository = characterRepo;
    }

    @Operation(summary = "Get all Characters")
    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters(){
        List<MovieCharacter> movieCharacters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieCharacters, status);
    }



    @Operation(summary = "Get a specific character using ID")
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable Long id){
        MovieCharacter movieCharacter = new MovieCharacter();
        HttpStatus status;
        //Checks if id exists
        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            movieCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(movieCharacter, status);
    }

    @Operation(summary = "Update specific Character using ID, and updated RequestBody")
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(@PathVariable Long id, @RequestBody MovieCharacter character){
        MovieCharacter updatedCharacter = new MovieCharacter();
        HttpStatus status;
        //Check if ID exists
        if (!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedCharacter, status);
        }
        updatedCharacter = characterRepository.save(character);
        status = HttpStatus.ACCEPTED;
        return new ResponseEntity<>(updatedCharacter, status);
    }

    @Operation(summary = "Add a new Character to the database, matching RequestBody")
    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter character){
        MovieCharacter newCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(newCharacter, status);
    }

    @Operation(summary = "Delete a specific Character using ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Long id){
        if (!characterRepository.existsById(id)){
            return new ResponseEntity<>("No Character with that ID exists!", HttpStatus.NOT_FOUND);
        }
        characterRepository.deleteById(id);
        return new ResponseEntity<>("Character with ID: " + id + " has been deleted.", HttpStatus.OK);
    }
}

