package theupsidedown.ludvigsfinalassignmentapi.controllers;

import theupsidedown.ludvigsfinalassignmentapi.domain.Franchise;
import theupsidedown.ludvigsfinalassignmentapi.Repository.FranchiseRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/sci-fi/franchises")
public class FranchiseController {
    @Autowired
    FranchiseRepository franchiseRepository;

    // GET all franchises
    @Operation(description = "Return all franchises")
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        return new ResponseEntity<>(franchiseRepository.findAll(), HttpStatus.OK);
    }

    // GET a franchise by id
    @Operation(description = "Return specific franchise by id given in path")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable long id){
        if(franchiseRepository.existsById(id)){
            return new ResponseEntity<>(franchiseRepository.findById(id).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    // POST a new franchise
    @Operation(description = "Add franchise to database")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        // Validate that the request body contains at least a name
        if(franchise.getName() != null) {
            Franchise newFranchise = franchiseRepository.save(franchise);
            return new ResponseEntity<>(newFranchise, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // DELETE a franchise with a specific id
    @Operation(description = "Delete franchise by id given in path.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteFranchise(@PathVariable long id){
        // Check if a franchise with that id exists in the database
        if(franchiseRepository.existsById(id)) {
            franchiseRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // PUT for replacing a franchise with a specific id
    @Operation(description = "Replace of add franchise by id")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> replaceFranchise(@PathVariable long id, @RequestBody Franchise franchise){
        // Check if the id in the path and the id in the request body matches
        if(id != franchise.getId()){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Validate that the request body contains at least a name
        if(franchise.getName() == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Franchise newFranchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(newFranchise, HttpStatus.OK);
    }

    // PATCH to update one or more fields in for a franchise with a specific id
    @Operation(description = "Update one or more fields in franchise by id given in path.")
    @PatchMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable long id, @RequestBody Franchise franchise){
        // Check if the id in the path and the id in the request body matches
        if(id != franchise.getId()){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Check if a franchise with that id exists in the database
        if(!franchiseRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        // Create a new franchise and fill it with information from the requestbody, then save to database
        var newFranchise  = franchiseRepository.findById(id).get();
        if(franchise.getName() != null){newFranchise.setName(franchise.getName());}
        if(franchise.getDescription() != null){newFranchise.setDescription(franchise.getDescription());}
        if(franchise.getMovies() != null){newFranchise.setMovies(franchise.getMovies());}
        newFranchise = franchiseRepository.save(newFranchise);
        return new ResponseEntity<>(newFranchise, HttpStatus.OK);
    }

    // PATCH for updating movies in a franchise.
    @Operation(description = "Update movies in franchise by id given in path. Ids for movies to update franchise given in body.")
    @PatchMapping("/{id}/movies")
    public ResponseEntity<Franchise> updateMoviesInFranchise(@PathVariable long id, @RequestBody int[] movieIds){
        // Check if a franchise with that id exists in the database
        if(!franchiseRepository.existsById(id)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        franchise.addMovies(movieIds);
        franchiseRepository.save(franchise);
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }

    /*// GET endpoint for all movies in a franchise
    @Operation(description = "Get all movies in franchise")
    @GetMapping("/{id}/movies")
    public ResponseEntity<FranchiseMoviesDto> getMoviesInFranchise(@PathVariable long id) {
        // Check if a franchise with that id exists in the database
        if(!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        var franchise = franchiseRepository.findById(id).get();
        return new ResponseEntity<>(franchiseMoviesDtoMapper(franchise), HttpStatus.OK);
    }

    // GET endpoint for all characters in a franchise
    @Operation(description = "Get all characters in franchise.")
    @GetMapping ("/{id}/characters")
    public ResponseEntity<FranchiseCharactersDto> getCharactersInFranchise(@PathVariable long id) {
        // Check if a franchise with that id exists in the database
        if(!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        var franchise = franchiseRepository.findById(id).get();
        return new ResponseEntity<>(franchiseCharactersDtoMapper(franchise), HttpStatus.OK);
    }*/
}

