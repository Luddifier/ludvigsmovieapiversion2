package theupsidedown.ludvigsfinalassignmentapi.domain;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class MovieCharacter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", nullable = false, length = 100)
    private String fullName;

    @Column(name = "alias", nullable = false, length = 100)
    private String alias;

    @Column(name = "gender", nullable = false, length = 10)
    private String gender;

    @Column(name = "imageUrl")
    private String imageUrl;

    @ManyToMany
    @JoinTable(
            name = "movie_characters",
            joinColumns = @JoinColumn(name = "characters_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id")
    )
    private List<Movie> movies;

    //Collecting movies to list for the link as url's.
    @JsonGetter("movies")
    public List<String> moviesGetter(){
        if (movies != null){
            return movies.stream()
                    .map(movie -> "/api/v1/movie/" + movie.getId())
                    .collect(Collectors.toList());
        }
        return null;
    }

    public MovieCharacter(long id, String fullName, String alias, String gender, String imageUrl) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.imageUrl = imageUrl;
    }

    public MovieCharacter() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }




}
