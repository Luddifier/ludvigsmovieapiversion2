package theupsidedown.ludvigsfinalassignmentapi.domain;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @Column(name = "title", nullable = false, length = 100)
    private String title;
    @Column(name = "genre", nullable = false, length = 100)
    private String genre;
    @Column(name = "release_year", nullable = false, length = 10)
    private int releaseYear;
    @Column(name = "director")
    private String director;
    @Column(name = "image_url")
    private String imageUrl;
    @Column(name = "trailer_url")
    private String trailerUrl;

    @ManyToOne
    @JoinColumn(name = "movie_franchise")
    private Franchise franchise;

    public Movie(long id, String title, String genre, int releaseYear, String director, String imageUrl, String trailerUrl, Franchise franchise, List<MovieCharacter> characters) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.imageUrl = imageUrl;
        this.trailerUrl = trailerUrl;
        this.franchise = franchise;
        this.characters = characters;
    }

    public Movie() {
    }

    @JsonGetter("franchise")
    public String franchise(){
        if(franchise != null){
            return "/api/sci-fi/franchises/" + franchise.getId();
        }
        return null;
    }

    @ManyToMany
    @JoinTable(name = "movie_characters",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "characters_id")})
    private List<MovieCharacter> characters = new ArrayList<>();

    @JsonGetter("characters")
    public List<String> characters() {
        if(characters != null) {
            return characters.stream().map(character -> {
                return "/api/sci-fi/characters/" + character.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public List<MovieCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(List<MovieCharacter> characters) {
        this.characters = characters;
    }

    public boolean checkIfCharactersExist(long id){
        for(MovieCharacter character : characters){
            if(character.getId() == id){
                return true;
            }
        }
        return false;
    }

    public void updateCharacters(int[] characterList) {
        characters.clear();
        for(int i:characterList){
            MovieCharacter character = new MovieCharacter();
            character.setId(i);
            characters.add(character);
        }
    }



}
