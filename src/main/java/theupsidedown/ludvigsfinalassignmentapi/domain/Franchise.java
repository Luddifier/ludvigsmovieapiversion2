package theupsidedown.ludvigsfinalassignmentapi.domain;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name", nullable = false, length = 100)
    private String name;
    @Column(name = "description")
    private String description;

    // List of movies in the franchise
    @OneToMany
    @JoinColumn(name = "movie_franchise")
    private List<Movie> movies;

    public Franchise(long id, String name, String description, List<Movie> movies) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.movies = movies;
    }

    @JsonGetter("movies")
    public List<String> movies(){
        if(movies != null){
            return movies.stream()
                    .map(movie ->{return "/api/sci-fi/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public Franchise() {
    }

    public Franchise(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public boolean checkIfMovieExist(long id){
        for(Movie movie: movies){
            if(movie.getId() == id){
                return true;
            }
        }
        return false;
    }

    public void addMovies(int[] movieIds) {
        movies.clear();
        for(int i: movieIds){
            var movie = new Movie();
            movie.setId(i);
            movies.add(movie);

        }
    }
}

