package theupsidedown.ludvigsfinalassignmentapi.Repository;


import theupsidedown.ludvigsfinalassignmentapi.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Movie Repo through Hibernate@
@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

}
