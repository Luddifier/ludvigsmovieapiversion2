package theupsidedown.ludvigsfinalassignmentapi.Repository;

import theupsidedown.ludvigsfinalassignmentapi.domain.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CharacterRepository extends JpaRepository<MovieCharacter, Long> {
}

