package theupsidedown.ludvigsfinalassignmentapi.Repository;

import theupsidedown.ludvigsfinalassignmentapi.domain.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Franchise Repo through Hibernate@
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
