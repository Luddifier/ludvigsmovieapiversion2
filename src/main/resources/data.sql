INSERT into franchise (id, description, name) VALUES (1, 'the man with the hat and the bullwhi', 'Indiana Jones' )

INSERT into movie (id, director, genre, image_url, release_year, title, trailer_url, movie_franchise) VALUES (1, 'Steven Spielberg', 'ActionAdventure', 'Image', 1986, 'Raiders of the Lost Ark', 'trailerLink', 1)

INSERT into movie_character (id, alias, full_name, gender, image_url) VALUES (1, 'Junior', 'Indiana Jones', 'Male', 'BULLWHIP')

INSERT into movie_characters (characters_id, movie_id) VALUES (1, 1)