package controllers;

import theupsidedown.ludvigsfinalassignmentapi.domain.MovieCharacter;
import theupsidedown.ludvigsfinalassignmentapi.Repository.CharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class characterController {
}

@RestController
@RequestMapping("api/sci-fi/characters")
class CharacterController {

    private final CharacterRepository characterRepo;

    @Autowired
    CharacterRepository characterRepository;

    public CharacterController(CharacterRepository characterRepo) {
        this.characterRepo = characterRepo;
    }

    // GET all characters currently stored in the database
    @Operation(description = "Returns all characters")
    @GetMapping
    public ResponseEntity<List<MovieCharacter>> getAllCharacters(){
        return new ResponseEntity<>(characterRepository.findAll(), HttpStatus.OK);
    }

    // GET a specific character by id
    @Operation(description = "Returns specific character by id assigned in path")
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable long id) {
        if(characterRepository.existsById(id)) {
            return new ResponseEntity<>(characterRepository.findById(id).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    // POST a new character
    @Operation(description = "Add character")
    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter character) {
        // Validate that the request body contains at least full name and gender
        if(character.getFullName() != null || character.getGender() != null) {
            MovieCharacter newCharacter = characterRepository.save(character);
            return new ResponseEntity<>(newCharacter, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // DELETE an existing character by id
    @Operation(description = "Delete character by id assigned in path")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteCharacter(@PathVariable long id) {
        if(characterRepository.existsById(id)) {
            characterRepository.deleteById(id);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    // PUT, replace an existing character by id
    @Operation(description = "Replace or add character by id")
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> replaceCharacter(@PathVariable long id, @RequestBody MovieCharacter character) {

        // Check if the id in the path and the id in the request body matches
        if (id != character.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Check if the request body contains at least full name and gender
        if(character.getFullName() == null || character.getGender() == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        MovieCharacter newCharacter = characterRepository.save(character);
        return new ResponseEntity<>(newCharacter, HttpStatus.OK);
    }

    // PATCH to update one or more fields for a specific character
    @Operation(description = "Update one or more fields for specific character given by id.")
    @PatchMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(@PathVariable long id, @RequestBody MovieCharacter character) {
        // Check if the id in the path and the id in the request body matches
        if (id != character.getId()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Check if a character with that id exists in the database
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        // Create a new character and fill it with information from the requestbody, then save to database
        var newCharacter = characterRepository.findById(id).get();
        if(character.getFullName() != null) {newCharacter.setFullName(character.getFullName());}
        if(character.getAlias() != null) {newCharacter.setAlias(character.getAlias());}
        if(character.getGender() != null) {newCharacter.setGender(character.getGender());}
        if(character.getImageUrl() != null) {newCharacter.setImageUrl(character.getImageUrl());}
        if(character.getMovies() != null){newCharacter.setMovies(character.getMovies());}
        characterRepository.save(newCharacter);
        return new ResponseEntity<>(newCharacter, HttpStatus.OK);
    }
}

